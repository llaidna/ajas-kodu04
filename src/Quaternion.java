import java.util.*;

/*
 * Expl of Q http://3dgep.com/understanding-quaternions/
 * 
 * Realiseerida abstraktne andmetüüp "kvaternioon" vastavalt kirjeldusele: 
 * http://enos.itcollege.ee/~jpoial/algoritmid/quat/ 
 * 
 *  Implement an abstract data type "Quaternion" according to specification: 
 *  http://enos.itcollege.ee/~jpoial/algoritmid/quat/
 *  
 *  Wiki https://en.wikipedia.org/wiki/Quaternion
 *  
 *  reaalarve ei saa == võrrelda, harva satuvad kokku
 *  
 *  AB on võrdsed siis, kui nende vahe absoluutväärtus on väga väike
 *  
 *  loengud kodutöö vihjete kohta:
 *  https://echo360.e-ope.ee/ess/echo/presentation/85c419cf-b068-417d-8bea-6adbc765a11f?ec=true
 *  https://echo360.e-ope.ee/ess/echo/presentation/ebe81bc7-c9c6-4714-bac9-129606cfef57?ec=true
 *  + 12 March 2015 loeng
 *  
 *  q = kompleksarvu üldistus
 *  objekt koosneb 4 reaalarvust
 *  hulk operatsioone vaja realiseerida
 *  
 *  0a. lahutamine
 *  0b. nulliga võrdumine - isZero (equals kasutab, inverse kasutab...)
 *  0c. equals
 *  2. equals        q1-q2 = isZero
 *  3. isZero saab igalpool kasutada (jagamisel jne)
 *  4. valueof ja toString teineteise paarilised, mida toString väljastab, peab valueOf lugeda oskama. Valueof võiks vale sisendstringi puhul exceptionis öelda vea (Vigane q või avaldis)!
 *  5. dokumentatsioonis on kirjas mis mida tähendab
 *  6. hashCode - ära näe liiga palju vaeva. Kasutatakse, et objekte näiteks HashMap'i paigutada ja köögipooletöid teha. 
 *     Peab equals objektide jaoks olema sama, equals erinevate objektide puhul olema erinev. Hea Hashcode POLE a+b+c+d täisarvus... Hashcode'i saab toStringist kätte = tostring!
 *     Ise tehes algarvudega... loe! ei ole hea idee hashcode'i fieldina q sisse panna. q sees ei taha lisavälju peale a+b+c+d. Peab kõigist abcd sõltuma!
 *  
 *  Lauri BitBucket - https://bitbucket.org/llaidna/ajas-kodu04
 */

/** Quaternions. Basic operations. */
public class Quaternion {
	
	// TODO!!! Your fields here!
//	private double r, i, j, k; // loengus seletatud nii!
	
	public static final double epsilon = 0.0000001; // täpsus  // Defineeri konstant eraldi.
	private final double a, b, c, d;
	
	
   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) { 
	   // konstruktor. Teeb seda, et initsialiseerib selle 4 reaalarvuline mälutükk ja 
	   // täidetakse vastavalt selliste algväärtustega nagu see konstruktor ütleb
//	   r = a; alternatiiv
//	   i = b; alternatiiv
	   this.a = a; // a - real part
	   this.b = b; // b - imaginary part i
	   this.c = c; // c - imaginary part j
	   this.d = d; // d - imaginary part k
   } // Quaternion constructor

   
   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return a; // või this.a ??
   }

   
   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return b;
   }

   
   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return c;
   }

   
   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return d;
   }

   
   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
	@Override
	public String toString() {
		// String string = Double.toString(a) + "+" + Double.toString(b) + "i" + Double.toString(c) + "j" + Double.toString(d) + "k";
		String string = Double.toString(a) + "r+";

		// järgnev teeb nii, et toString miinuse puhul lisab ainult miinuse elementide vahele.
		// if (b >= 0) string += "+";
		// string += Double.toString(b) + "i";
		//
		// if (c >= 0) string += "+";
		// string += Double.toString(c) + "j";
		//
		// if (d >= 0) string += "+";
		// string += Double.toString(d) + "k";

		string += Double.toString(b) + "i+";
		string += Double.toString(c) + "j+";
		string += Double.toString(d) + "k";
		return string;
	} // toString

	
   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
	public static Quaternion valueOf(String s) {
		String[] strings = s.split("\\+");

		for (int i = 0; i < strings.length; i++) {
			strings[i] = strings[i].replaceAll("r", "");
			strings[i] = strings[i].replaceAll("i", "");
			strings[i] = strings[i].replaceAll("j", "");
			strings[i] = strings[i].replaceAll("k", "");
		}
		
		if (strings.length != 4) {
			throw new RuntimeException("Mitte ei suuda parsida. Quaternion peab olema sisestatud kujul \"0.0r+0.0i+0.0j+0.0k\" või \"-0.0r+-0.0i+-0.0j+-0.0k\" jne, sina sisestasid: " + s);
		}
		
		Quaternion valueOf = null;
		try {
			valueOf = new Quaternion(Double.parseDouble(strings[0]), Double.parseDouble(strings[1]), Double.parseDouble(strings[2]), Double.parseDouble(strings[3]));
		} catch (NumberFormatException e) {
			System.out.println("Mitte ei suuda parsida. Quaternion peab olema sisestatud kujul \"0.0r+0.0i+0.0j+0.0k\" või \"-0.0r+-0.0i+-0.0j+-0.0k\" jne, sina sisestasid: " + s);
			e.printStackTrace();
		}
		return valueOf; // TODO!!!
	}

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
	   Quaternion qClone = new Quaternion (this.a, this.b, this.c, this.d);
	   return qClone; // TODO!!!
   } // clone

   
	/**
	 * Test whether the quaternion is zero.
	 * 
	 * @return true, if the real part and all the imaginary parts are (close to) zero
	 */
	public boolean isZero() { // tee esimeseks !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// public static final double epsilon = 0.000001; // täpsus // Defineeri konstant eraldi.
		// edasi kasutame konstanti epsilon alati ja ainult!
		if (Math.abs(a) < epsilon && Math.abs(b) < epsilon && Math.abs(c) < epsilon && Math.abs(d) < epsilon) {
			return true;
		} else {
			return false; // TODO!!!
		}
	} // isZero

	
   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
	public Quaternion conjugate() {
		Quaternion qConjugate = new Quaternion(this.a, this.b*(-1), this.c*(-1), this.d*(-1));
		return qConjugate;
	} // conjugate

	
   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
	   Quaternion qOpposite = new Quaternion(this.a*(-1), this.b*(-1), this.c*(-1), this.d*(-1));
		return qOpposite;
   } // opposite

   
   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
		Quaternion plusQ = new Quaternion((this.a + q.a), (this.b + q.b), (this.c + q.c), (this.d + q.d));
		return plusQ;
   } // plus

   
   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = 
    *  (a1a2-b1b2-c1c2-d1d2) + 
    *  (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + 
    *  (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
	public Quaternion times(Quaternion q) {
		// esimene sulg on esimene q, selle korrutamine teise q ehk teise suluga = tähendab seda, 
		// et moodustatakse uus q, mille komponendid arvutatakse nii, et tulemuse reealosa () + i komponent () +
		// j komponent () + k komponent ()
		Quaternion timesQ = new Quaternion(
				(this.a*q.a-this.b*q.b-this.c*q.c-this.d*q.d), 
				(this.a*q.b+this.b*q.a+this.c*q.d-this.d*q.c), 
				(this.a*q.c-this.b*q.d+this.c*q.a+this.d*q.b), 
				(this.a*q.d+this.b*q.c-this.c*q.b+this.d*q.a)
				);
		return timesQ;
	} // times

	
   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
	public Quaternion times(double r) {
		Quaternion times = new Quaternion(this.a*r, this.b*r, this.c*r, this.d*r);
		return times;
	} // times double

	
   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = 
    *        a /(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + 
    *     ((-c)/(a*a+b*b+c*c+d*d))j + 
    *     ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
	public Quaternion inverse() {
		
		if (this.isZero()) {
			throw new RuntimeException("inversitav quaternion ei tohi sisaldada 0-väärtusi. Viga tekitav sisend: " + this.toString());
		}
		
		Quaternion qInverse = new Quaternion(
						(((a    /(a*a+b*b+c*c+d*d)))), 
						((b*-1)/(a*a+b*b+c*c+d*d)),
						((c*-1)/(a*a+b*b+c*c+d*d)),
						((d*-1)/(a*a+b*b+c*c+d*d))
						);

		return qInverse;
	} // inverse

	
   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
	public Quaternion minus(Quaternion q) {
		Quaternion minusQ = new Quaternion((this.a - q.a), (this.b - q.b), (this.c - q.c), (this.d - q.d));
		return minusQ;
	} // minus

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      return this.times(q.inverse());
   } // divideByRight

   
   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
	public Quaternion divideByLeft(Quaternion q) {
		return q.inverse().times(this);
	} // divideByLeft

	
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
	@Override
	public boolean equals (Object qo) { // |q1 - q2| = epsilon OLULINE ÄRA TEHA AGLUSES!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		return this.minus((Quaternion)qo).isZero(); 

		//		Quaternion qQo = (Quaternion) qo;
//		Quaternion qMuu = new Quaternion(a, b, c, d);
//		if (qMuu.minus(qQo).isZero()) {
//			System.out.println("equals " + qMuu.toString() + " " + qQo.toString());
//			return true;
//		} else {
//			System.out.println("equals oli false!!! " + qMuu.toString() + " " + qQo.toString());
//			return false;
//		}
//		System.out.println("equals test qQo  " + qQo.toString());
//		System.out.println("equals test qMuu " + qMuu.toString());
//		System.out.println("equals meetodist qo: " + qo + " ja this: " + this);
//		if (qo.toString().hashCode() - this.hashCode() <= epsilon) {
//			return true;
//		} else {
//			return false;
//		}
	} // equals

	
   /** Dot product of quaternions. ( p*conjugate(q) + q*conjugate(p) ) / 2
    * @param q factor
    * @return dot product of this and q
    */
	public Quaternion dotMult(Quaternion q) {
//		double pool = 0.5;
		return this.times(q.conjugate()).plus(q.times(this.conjugate())).times(0.5);
	}

	
   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return this.toString().hashCode(); // this.toString(); põhimõtteliselt võib kuidagi umbes nii teha, kell 18.22 rääkis 12march2015
   } // hashCode

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(a*a+b*b+c*c+d*d);
   } // norm

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
	public static void main(String[] arg) {

//		Quaternion ise1 = new Quaternion(0, 0, 0, 0);
//		System.out.println("iszero test: " + ise1.isZero());
		
		Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
		if (arg.length > 0)
			arv1 = valueOf(arg[0]);
		System.out.println("toString: " + arv1.toString());
		System.out.println("real: " + arv1.getRpart());
		System.out.println("imagi: " + arv1.getIpart());
		System.out.println("imagj: " + arv1.getJpart());
		System.out.println("imagk: " + arv1.getKpart());
		System.out.println("isZero: " + arv1.isZero());
		System.out.println("conjugate: " + arv1.conjugate());
		System.out.println("opposite: " + arv1.opposite());
		System.out.println("hashCode: " + arv1.hashCode());
//		Quaternion res = null;
//		try {
//			res = (Quaternion) arv1.clone();
//		} catch (CloneNotSupportedException e) {
//		}
//		;
//		System.out.println("clone equals to original: " + res.equals(arv1));
//		System.out.println("clone is not the same object: " + (res != arv1));
//		System.out.println("hashCode: " + res.hashCode());
//		res = valueOf(arv1.toString());
//		System.out.println("string conversion equals to original: " + res.equals(arv1));
//		Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
//		if (arg.length > 1)
//			arv2 = valueOf(arg[1]);
//		System.out.println("second: " + arv2.toString());
//		System.out.println("hashCode: " + arv2.hashCode());
//		System.out.println("equals: " + arv1.equals(arv2));
//		res = arv1.plus(arv2);
//		System.out.println("plus: " + res);
//		System.out.println("times: " + arv1.times(arv2));
//		System.out.println("minus: " + arv1.minus(arv2));
//		double mm = arv1.norm();
//		System.out.println("norm: " + mm);
//		System.out.println("inverse: " + arv1.inverse());
//		System.out.println("divideByRight: " + arv1.divideByRight(arv2));
//		System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
//		System.out.println("dotMult: " + arv1.dotMult(arv2));
	}
}
// end of file
